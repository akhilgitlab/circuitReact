import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import './Login.css';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                username: '',
                password: ''
            },
            clicked: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }

    handleChange(events){
        const { name, value } = events.target;
        const { user } = this.state;
        user[name] = value;
        this.setState({
            user: user
        })
    }

    handleLogin(events) {
        this.props.history.push(`/circuit`)
    }

    render() {
        const { user, clicked } = this.state
        return (
            <div>
                <div className="loginPage">
                    <h2>Login</h2>
                    <div className="loginBody">
                        <div className={'form-group' + (!user.username ? ' has-error' : '')}>
                            <label htmlFor="username">Username: </label>
                            <input type="text" className="form-control" name="username" value={user.username} onChange={this.handleChange} placeholder="enter your user name"/>
                            {clicked && !user.username &&
                                <div className="help-block">Username is required</div>
                            }
                        </div>
                        <div className={'form-group' + (!user.password ? ' has-error' : '')}>
                            <label htmlFor="password">Password: </label>
                            <input type="password" className="form-control" name="password" value={user.password} onChange={this.handleChange} placeholder="enter your password"/>
                            {clicked && !user.password &&
                                <div className="help-block">Password is required</div>
                            }
                        </div>
                        <div className="form-group">
                        <button className="btn btn-primary btn-common" onClick={this.handleLogin}>Login</button>
                        <Link to="/reg" className="btn btn-link">New Registration</Link>
                    </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Login;