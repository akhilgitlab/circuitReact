import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './Circuit.css';

class Circuit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render(){
        return (
            <div className="circuitPage">
                <div className="leftSelector">
                    <ul>
                        <li>AND</li>
                        <li>OR</li>
                        <li>XOR</li>
                        <li>NAND</li>
                        <li>NOR</li>
                        <li>XNOR</li>
                        <li>INVERTER</li>
                    </ul>
                </div>
                <div className="rightSelector">
                    <div className="tabGrid">
                    </div>
                </div>
            </div>
        );
    }
}

// ReactDOM.render(<Grid />,
//   document.getElementById('root')
// );

export default Circuit;