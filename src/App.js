import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Login from './login/Login';
import RegisterPage from './registration/Reg';
import Circuit from './circuits/Circuit';
import './App.css';

class App extends React.Component {

  render() {
      return (
          <Router>
                <div className="appPage">
                    <h2>Circuit Task</h2>
                    <hr/>
                    
                    <Switch>
                        <Route exact path='/reg' component={RegisterPage} />
                        <Route exact path='/login' component={Login} />
                        <Route exact path='/circuit' component={Circuit} />
                    </Switch>
                </div>
            </Router>
      );
  }

}

export default App;
